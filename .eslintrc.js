module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	plugins: ['@typescript-eslint', 'prettier'],
	extends: [
		'airbnb-typescript/base',
		'plugin:prettier/recommended',
		'prettier/@typescript-eslint',
	],
	parserOptions: {
		project: './tsconfig.json',
	},
};
