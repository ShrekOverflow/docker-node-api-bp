import winston from 'winston';

const logger = winston.createLogger({
	transports: [
		new winston.transports.Console({
			format: winston.format.combine(
				winston.format.json(),
				winston.format.timestamp(),
				winston.format.prettyPrint({
					colorize: true,
				}),
			),
			level: 'debug',
			handleExceptions: true,
		}),
	],
	exitOnError: true,
});

export default logger;
