import * as env from 'env-var';

// Helpers should only be used within this file
const NODE_ENV = env.get('NODE_ENV').asString();
const IS_PROD = NODE_ENV === 'production';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IS_DEV = !IS_PROD;

export const USE_OPENAPI_VALIDATION = env.get('USE_OPENAPI_VALIDATION').default(0).asBool();

// App Specific Vars
export const REQUEST_LOG_FORMAT = env
	.get('REQUEST_LOG_FORMAT')
	.default(IS_PROD ? 'apache' : 'tiny')
	.asEnum(['apache', 'combined', 'short', 'tiny', 'dev']);

export const PORT = env.get('PORT').default(3000).asInt();
export const HOSTNAME = env.get('HOSTNAME').default('loopback').asString();
