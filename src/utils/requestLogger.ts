import morgan from 'morgan';
import logger from './logger';
import { REQUEST_LOG_FORMAT } from './env';

const morganstream = {
	write(message: string) {
		logger.info(message.trim());
	},
};

export default morgan(REQUEST_LOG_FORMAT, {
	stream: morganstream,
});
