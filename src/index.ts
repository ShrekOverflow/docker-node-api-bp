import { OpenApiValidator } from 'express-openapi-validator';
import Express from 'express';

import requestLogger from './utils/requestLogger';
import router from './routes';
import { USE_OPENAPI_VALIDATION } from './utils/env';

export default async function createApp() {
	const app = Express();
	app.use(requestLogger);

	if (USE_OPENAPI_VALIDATION) {
		const validator = new OpenApiValidator({
			apiSpec: 'defs/api.v1.yaml',
			validateRequests: true,
			validateResponses: true,
		});

		await validator.install(app);
	}
	app.use(router);

	return app;
}
