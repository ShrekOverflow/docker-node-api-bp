import { Server as WebsocketServer } from 'ws';
import * as http from 'http';

export default function createWebsocketServer(server: http.Server) {
	const wsServer = new WebsocketServer({
		server,
	});
	return wsServer;
}
