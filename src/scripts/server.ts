import http from 'http';
import { PORT, HOSTNAME } from '../utils/env';
import logger from '../utils/logger';
import createApp from '..';
import createWebsocketServer from 'src/websockets';

(async function runServer() {
	const app = await createApp();
	const server = http.createServer(app);
	const wsServer = createWebsocketServer(server);
	server.listen(PORT, HOSTNAME, () => {
		logger.log('info', `Server running on ${HOSTNAME}:${PORT}`);
		logger.log('info', `WS Server running ${wsServer}`);
	});
})();
