FROM node:14-alpine as base

RUN apk add --update --no-cache \
	curl \
    python \
    make \
    g++ \
    git

WORKDIR /usr/src/app
COPY ./*.json ./

# DEVELOPMENT
FROM base as development
RUN npm install --quiet --no-progress

# PRODUCTION
FROM base as builder 

RUN npm ci --quiet --no-progress
COPY ./src /usr/src/app/src
RUN npm run build
RUN npm prune --production

# @Todo: Make this work, why is it not is beyond me (atm)
# RUN curl -sf https://gobinaries.com/tj/node-prune | sh
# RUN /usr/local/bin/node-prune

FROM node:14-alpine as prod

WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/node_modules node_modules
COPY --from=builder /usr/src/app/lib lib
COPY ./defs /usr/src/app/defs